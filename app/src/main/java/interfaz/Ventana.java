package interfaz;

import java.awt.Color;                  // Utilizar colores determinados (para modificar aspecto de la interfaz)
import java.awt.Font;                   // Tipo de fuente
import java.awt.Image;                  // Modifica las propiedades de una imagen (por ej: dimensiones)
import java.awt.event.*;                // Oyentes (registra el mouse, teclado y acciones en general)
import javax.swing.JFrame;              // Interfaz
import javax.swing.JLabel;              // Etiqueta (lineas de texto basicas)
import javax.swing.JPanel;              // Panel (donde se ponen los componentes como etiquetas, cajas de texto, etc)
import javax.swing.JTextField;          // Linea de texto en la que el usuario puede escribir
import javax.swing.JTextArea;           // Multilinea de texot
import javax.swing.SwingConstants;      // Constantes (utilizadas para posicionar correctamente componentes)
import javax.swing.ImageIcon;           // Utilizar imagenes
import javax.swing.JButton;             // Botones
import javax.swing.JComboBox;           // Lista deplegable

public class Ventana extends JFrame
{
    JPanel panel;                               // Atributo. El panel no se declara en un metodo ya que
                                                // es utilizado en todos los metodos (se facilita usarlo como atributo)

    public Ventana(int ancho, int largo)        // Constructor
    {
        setSize(ancho,largo); // Tamaño (Ancho, Largo) de la ventana princiapl
        //setLocation(100,100); || Ubicacion
        // setBounds(x, y, ancho, largo); || Engloba a los metodos setSize y setLocation en uno solo

        setTitle("Titulo de la ventana");

        setLocationRelativeTo(null); // Ubicacion relativa. Si se usa "null", queda centrada


        // Llamada a metodos privados. Siempre utiliza el de inicializarComponentesDeVentana().
        // el resto de metodos es recomendable utilizar uno a la vez para que se vean correctamente al ejecutar
        inicializarComponentesDeVentana();
        //colocarEtiquetas();
        //colocarBotones();
        //eventoOyenteBoton();
        //colocarCajasDeTexto();
        //colocarAreasDeTexto();
        //eventoOyenteRaton();
        //eventoOyenteTeclado();
        //colocarListasDeplegables();
        
        setDefaultCloseOperation(EXIT_ON_CLOSE); // Programa deja de ejecutarse al cerrar la ventana
    }

    private void inicializarComponentesDeVentana()  //Se inicializa el panel, donde ingresamos el resto de componentes
    {
        panel = new JPanel();
        panel.setLayout(null); // Desactivado el diseño por defecto del panel (permite mayor control de los componentes como ubicarlos o modificarlos)
        panel.setBackground(Color.LIGHT_GRAY);  // Darle un color de fondo al panel (utiliza la clase Color)
        this.getContentPane().add(panel); //Añadir panel a ventana 
        
        
    }

    private void colocarEtiquetas()
    {
        // Etiqueta texto
        JLabel etiqueta = new JLabel(); 
        etiqueta.setText("Texto"); // Se establece el texto de la etiqueta
        etiqueta.setHorizontalAlignment(SwingConstants.CENTER); // Centrar texto con respecto a su ubicacion
        etiqueta.setBounds(50,10, 100, 20); // Coordenadas y tamaño (x,y,ancho,largo)
        etiqueta.setForeground(Color.BLUE); // Color de texto
        etiqueta.setFont(new Font("Verdana",Font.BOLD,20)); // Fuente de texto (tipo,estilo,tamaño) || Se utiliza clase Font
        // Link con los tipos de fuentes: https://alvinalexander.com/blog/post/jfc-swing/swing-faq-list-fonts-current-platform/
        etiqueta.setOpaque(true); // Se da permiso a la opcion de añadir un color de fondo al texto
        etiqueta.setBackground(Color.WHITE); // Color de fondo de texto
    
        panel.add(etiqueta); //Se añade la etiqueta al panel



        // Etiqueta imagen
        ImageIcon imagen = new ImageIcon("ImagenPrueba.png");
        JLabel etiquetaImagen = new JLabel(imagen,SwingConstants.CENTER);
        // Inicializada con una imagen guardada en la direccion prueba-de-jframe/app. Centrada

        etiquetaImagen.setBounds(50, 50, 400, 300);

        etiquetaImagen.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(400, 300, Image.SCALE_SMOOTH))); 
        // Escalado suave de imagen
       
        panel.add(etiquetaImagen);
    }

    private void colocarBotones()
    {
        // Boton con texto
        JButton boton1 = new JButton();
        boton1.setText("Haceme click"); // Texto del boton
        boton1.setBounds(200, 10, 170, 40);
        boton1.setEnabled(true); // Permite interactuar con el boton (en caso de false, no se puede clickear)
        
        boton1.setMnemonic('a'); // (Alt + a) activa el boton
        boton1.setForeground(Color.MAGENTA);
        boton1.setFont(new Font("arial",Font.ITALIC,20));
        panel.add(boton1);

        

        // Boton con imagen
        JButton boton2 = new JButton();
        boton2.setBounds(10,40,150,40);
        ImageIcon click = new ImageIcon("PruebaClick.png");
        boton2.setIcon(new ImageIcon(click.getImage().getScaledInstance(boton2.getWidth(),boton2.getHeight(), Image.SCALE_SMOOTH)));
        panel.add(boton2);
    }
    private void colocarCajasDeTexto()
    {
        JTextField cajaDeUnaLinea = new JTextField();
        cajaDeUnaLinea.setBounds(20,20,100,30);
        cajaDeUnaLinea.setText("Buenas");
        panel.add(cajaDeUnaLinea);
    }
    private void colocarAreasDeTexto() // Escribir en dos o mas lineas
    {
        JTextArea areaTexto = new JTextArea();
        areaTexto.setBounds(20,20,200,200);
        areaTexto.setText("Las aventuras del programador ");
        areaTexto.append("\nque le daba paja el lab de arq \ny de prog,");
        areaTexto.append("\nasi que se puso a boludear");

        areaTexto.setEditable(true); // Permite editar/remover texto

        System.out.println("El texto de la caja es " + areaTexto.getText());
        panel.add(areaTexto);
    }
    private void colocarListasDeplegables()
    {
        String [] lista = {"Boludez","Cosita","Nose"};
        JComboBox<String> listaDeplegable = new JComboBox<String>(lista);
        listaDeplegable.setBounds(20,20,100,30);

        listaDeplegable.addItem("Pelotudez");

        listaDeplegable.setSelectedItem("Cosita");
        panel.add(listaDeplegable);
    }

    private void eventoOyenteBoton()
    {
        // Boton con texto
        JButton boton1 = new JButton();
        boton1.setText("Haceme click"); // Texto del boton
        boton1.setBounds(200, 10, 170, 40);
        boton1.setEnabled(true); // Permite interactuar con el boton (en caso de false, no se puede clickear)
        
        boton1.setMnemonic('a'); // (Alt + a) activa el boton
        boton1.setForeground(Color.MAGENTA);
        boton1.setFont(new Font("arial",Font.ITALIC,20));
        panel.add(boton1);

        JLabel etiqueta = new JLabel(); 
        etiqueta.setHorizontalAlignment(SwingConstants.CENTER);
        etiqueta.setBounds(50,100, 300, 20);
        etiqueta.setText("Ola");
        panel.add(etiqueta);

        ActionListener oyenteBoton = new ActionListener()
        {
            @Override
            public void actionPerformed (ActionEvent e)
            {
                etiqueta.setText("Gracias por apretarme");
            }
        };
        boton1.addActionListener(oyenteBoton);
    }
    private void eventoOyenteRaton()
    {
        JTextArea areaTexto = new JTextArea();
        areaTexto.setBounds(20,20,400,400);
        areaTexto.setText("");

        panel.add(areaTexto);
        MouseListener oyenteRaton = new MouseListener()
        {
         
            @Override
            public void mouseClicked(MouseEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Click\n");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Click apretado\n");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Click soltado\n");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Mouse ingreso\n");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Mouse salio\n");
            }

        };
        areaTexto.addMouseListener(oyenteRaton);
    }
    private void eventoOyenteTeclado()
    {
        JTextArea areaTexto = new JTextArea();
        areaTexto.setBounds(20,20,400,400);
        areaTexto.setText("");
        panel.add(areaTexto);
        KeyListener oyenteTeclado = new KeyListener()
        {
            @Override
            public void keyTyped(KeyEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("\nTecla tipeada\n");
                if (e.getKeyChar() == 'a')
                {
                    areaTexto.append("Tipeaste la letra a");
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Tecla presionada\n");
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // TODO Auto-generated method stub
                areaTexto.append("Tecla soltada\n");
            }
        };
        areaTexto.addKeyListener(oyenteTeclado);
    }
}


